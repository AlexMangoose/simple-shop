import {ActionReducerMap} from '@ngrx/store';
import * as productsReducer from '../components/products/store/products.reducer';

export interface AppState {
  products: productsReducer.ProductsState;
}

export const appReducer: ActionReducerMap<AppState> = {
  products: productsReducer.ProductsReducer
};
