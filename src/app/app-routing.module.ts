import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './components/products/list/list.component';
import {DetailComponent} from './components/products/detail/detail.component';
import {BasketComponent} from './components/products/basket/basket.component';
import {ProductsResolver} from './components/products/list/products.resolver';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'products',
    pathMatch: 'full'
  },
  {
    path: 'products',
    component: ListComponent,
    resolve: [ProductsResolver]
  },
  {
    path: 'products/detail/:id',
    component: DetailComponent,
  },
  {
    path: 'basket',
    component: BasketComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
