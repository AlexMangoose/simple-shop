import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import * as fromApp from './appStore/app.reducer';
import {Observable} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {map} from 'rxjs/operators';
import * as ProductsActions from './components/products/store/products.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  basketSum$: Observable<number>;
  form: FormGroup;

  constructor(private store: Store<fromApp.AppState>) {
    this.form = new FormGroup({
      search: new FormControl()
    });
  }

  ngOnInit() {
    this.basketSum$ = this.store.select('products', 'basketSum');

    this.form.valueChanges.pipe(
      map((input) => {
        this.store.dispatch(new ProductsActions.UpdateSearch(input.search));
      })
    ).subscribe();
  }
}
