export interface Product {
  'id': number;
  'images': {
    'large': string;
    'small': string
  };
  'title': string;
  'description': string;
  'price': {
    'value': number;
    'currency': {
      'id': number;
      'code': string;
      'symbol': string
    }
  };
  'likes': number;
}
