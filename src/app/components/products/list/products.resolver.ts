import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable, of} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromApp from '../../../appStore/app.reducer';
import * as ProductsActions from '../store/products.actions';
import {Actions, ofType} from '@ngrx/effects';
import {first, switchMap} from 'rxjs/operators';
import {Product} from '../../../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsResolver implements Resolve<Product[]> {
  constructor(private store: Store<fromApp.AppState>, private actions$: Actions) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product[]> {
    return this.store.select('products', 'products').pipe(
      first(),
      switchMap((products: Product[]) => {
        if (products.length === 0) {
          this.store.dispatch(new ProductsActions.FetchProducts());
          return this.actions$.pipe(
            ofType(ProductsActions.SET_PRODUCTS),
            first(),
          );
        } else {
          return of(products);
        }
      })
    );
  }
}
