import {Component, OnInit} from '@angular/core';
import {Product} from '../../../models/product.model';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import * as fromApp from '../../../appStore/app.reducer';
import * as ProductsActions from '../store/products.actions';
import {map, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  title = 'simple-shop';
  products$: Observable<Product[]>;

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit() {
    this.store.select('products', 'search').pipe(
      switchMap((search: string) => {
        return this.products$ = this.store.select('products', 'products')
          .pipe(map((products: Product[]) => {
              return (search !== undefined && search.length > 1) ? products
                .filter(patient =>
                  patient.title.toLowerCase().includes(search) ||
                  patient.description.toLowerCase().includes(search)
                ) : products;
            })
          );
      })
    ).subscribe();
  }

  onAddToBasket(product: Product) {
    this.store.dispatch(new ProductsActions.AddToBasket(product));
  }

  onAddLike(product: Product) {
    this.store.dispatch(new ProductsActions.AddLike(product));
  }
}
