import {Component, OnInit} from '@angular/core';
import {BasketItem} from '../../../models/basket-item.model';
import {Store} from '@ngrx/store';
import * as fromApp from '../../../appStore/app.reducer';
import * as ProductsActions from '../store/products.actions';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
  basketItems$: Observable<BasketItem[]>;

  constructor(private store: Store<fromApp.AppState>) {
  }

  ngOnInit(): void {
    this.basketItems$ = this.store.select('products', 'basket');
  }

  onRemoveFromBasket(basketItem: BasketItem) {
    this.store.dispatch(new ProductsActions.RemoveFromBasket(basketItem));
  }

}
