import * as ProductActions from './products.actions';
import {Product} from '../../../models/product.model';
import {BasketItem} from '../../../models/basket-item.model';

export interface ProductsState {
  products: Product[];
  basket: BasketItem[];
  basketSum: number;
  search: string;
}

const initialState: ProductsState = {
  products: [],
  basket: [],
  basketSum: 0,
  search: ''
};

export function ProductsReducer(
  state = initialState,
  action: ProductActions.ProductsActions) {
  switch (action.type) {
    case ProductActions.SET_PRODUCTS:
      return {
        ...state,
        products: [...action.payload]
      };
    case ProductActions.ADD_TO_BASKET:
      let basket: BasketItem[] = [];
      if (state.basket.some(i => i.product.id === action.payload.id)) {
        state.basket.forEach(i => {
          basket.push({product: i.product, count: i.product.id === action.payload.id ? (i.count + 1) : i.count});
        });
      } else {
        basket = [...state.basket, {product: action.payload, count: 1}];
      }
      let basketSum = 0;
      basket.forEach(i => basketSum = basketSum + (i.count * i.product.price.value));
      return {
        ...state,
        basket: [...basket],
        basketSum: Number(getSum(basket))
      };
    case ProductActions.REMOVE_FROM_BASKET:
      return {
        ...state,
        basket: [...state.basket.filter(bi => bi.product.id !== action.payload.product.id)],
        basketSum: Number(getSum(state.basket.filter(bi => bi.product.id !== action.payload.product.id)))
      };
    case ProductActions.ADD_LIKE:
      return {
        ...state,
        products: [...state.products
          .map(p => p.id === action.payload.id ? {...p, likes: p.likes + 1} : {...p})]
      };
    case ProductActions.UPDATE_SEARCH:
      return {
        ...state,
        search: String(action.payload)
      };
    default:
      return state;
  }
}

function getSum(basket: BasketItem[]) {
  return basket.reduce((prev, curr) => {
    return prev + (curr.product.price.value * curr.count);
  }, 0);
}
