import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {HttpClient} from '@angular/common/http';
import * as ProductsActions from './products.actions';
import {map, switchMap} from 'rxjs/operators';
import {Product} from '../../../models/product.model';

@Injectable()
export class ProductsEffects {
  constructor(
    private actions$: Actions,
    private http: HttpClient
  ) {
  }

  @Effect()
  fetchPatients = this.actions$.pipe(
    ofType(ProductsActions.FETCH_PRODUCTS),
    switchMap(() => {
      return this.http.get<Product[]>('https://simple-shop.docker.dev04.b2a.cz/products');
    }),
    map((products: Product[]) => {
      return new ProductsActions.SetProducts(products);
    })
  );
}
