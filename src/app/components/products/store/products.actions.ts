import {Action} from '@ngrx/store';
import {Product} from '../../../models/product.model';
import {BasketItem} from '../../../models/basket-item.model';

export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export const SET_PRODUCTS = 'SET_PRODUCTS';
export const ADD_TO_BASKET = 'ADD_TO_BASKET';
export const REMOVE_FROM_BASKET = 'REMOVE_FROM_BASKET';
export const ADD_LIKE = 'ADD_LIKE';
export const UPDATE_SEARCH = 'UPDATE_SEARCH';

export class FetchProducts implements Action {
  readonly type = FETCH_PRODUCTS;
}

export class SetProducts implements Action {
  readonly type = SET_PRODUCTS;

  constructor(public payload: Product[]) {
  }
}

export class AddToBasket implements Action {
  readonly type = ADD_TO_BASKET;

  constructor(public payload: Product) {
  }
}

export class RemoveFromBasket implements Action {
  readonly type = REMOVE_FROM_BASKET;

  constructor(public payload: BasketItem) {
  }
}

export class AddLike implements Action {
  readonly type = ADD_LIKE;

  constructor(public payload: Product) {
  }
}

export class UpdateSearch implements Action {
  readonly type = UPDATE_SEARCH;

  constructor(public payload: string) {
  }
}

export type ProductsActions =
  | FetchProducts
  | SetProducts
  | AddToBasket
  | RemoveFromBasket
  | AddLike
  | UpdateSearch;
