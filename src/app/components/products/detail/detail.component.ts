import {Component, OnInit} from '@angular/core';
import {Product} from '../../../models/product.model';
import {ActivatedRoute} from '@angular/router';
import {Store} from '@ngrx/store';
import * as fromApp from '../../../appStore/app.reducer';
import * as ProductsActions from '../store/products.actions';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  product: Product;

  constructor(private store: Store<fromApp.AppState>, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.store.select('products', 'products')
      .subscribe((products: Product[]) => this.product = products
        .find(p => p.id.toString() === this.route.snapshot.paramMap.get('id')));
  }

  onAddToBasket(product: Product) {
    this.store.dispatch(new ProductsActions.AddToBasket(product));
  }

  onAddLike(product: Product) {
    this.store.dispatch(new ProductsActions.AddLike(product));
  }
}
